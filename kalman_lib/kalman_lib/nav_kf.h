﻿#ifndef _nav_kf_h
#define _nav_kf_h

#include "../../kalman_core/srcdkf_low.h"


#define GRAVITY		9.80665f	// m/s^2


#define SIM_S                   16		// states
#define SIM_M                   3		// max measurements
#define SIM_V                   12		// process noise
#define SIM_N                   3		// max observation noise

#define UKF_STATE_VELX		0
#define UKF_STATE_VELY		1
#define UKF_STATE_VELZ		2
#define UKF_STATE_POSX		3
#define UKF_STATE_POSY		4
#define UKF_STATE_POSZ		5
#define UKF_STATE_Q1		6
#define UKF_STATE_Q2		7
#define UKF_STATE_Q3		8
#define UKF_STATE_Q4		9
#define UKF_STATE_ACC_BIAS_X	10
#define UKF_STATE_ACC_BIAS_Y	11
#define UKF_STATE_ACC_BIAS_Z	12
#define UKF_STATE_GYO_BIAS_X	13
#define UKF_STATE_GYO_BIAS_Y	14
#define UKF_STATE_GYO_BIAS_Z	15


#define UKF_V_NOISE_VEL_X	0
#define UKF_V_NOISE_VEL_Y	1
#define UKF_V_NOISE_VEL_Z	2
#define UKF_V_NOISE_RATE_X	3
#define UKF_V_NOISE_RATE_Y	4
#define UKF_V_NOISE_RATE_Z	5
#define UKF_V_NOISE_ACC_BIAS_X	6
#define UKF_V_NOISE_ACC_BIAS_Y	7
#define UKF_V_NOISE_ACC_BIAS_Z	8
#define UKF_V_NOISE_GYO_BIAS_X	9
#define UKF_V_NOISE_GYO_BIAS_Y	10
#define UKF_V_NOISE_GYO_BIAS_Z	11


#define UKF_VELX		x[UKF_STATE_VELX]
#define UKF_VELY		x[UKF_STATE_VELY]
#define UKF_VELZ		x[UKF_STATE_VELZ]
#define UKF_POSX		x[UKF_STATE_POSX]
#define UKF_POSY		x[UKF_STATE_POSY]
#define UKF_POSZ		x[UKF_STATE_POSZ]
#define UKF_ACC_BIAS_X		x[UKF_STATE_ACC_BIAS_X]
#define UKF_ACC_BIAS_Y		x[UKF_STATE_ACC_BIAS_Y]
#define UKF_ACC_BIAS_Z		x[UKF_STATE_ACC_BIAS_Z]
#define UKF_GYO_BIAS_X		x[UKF_STATE_GYO_BIAS_X]
#define UKF_GYO_BIAS_Y		x[UKF_STATE_GYO_BIAS_Y]
#define UKF_GYO_BIAS_Z		x[UKF_STATE_GYO_BIAS_Z]
#define UKF_Q1			x[UKF_STATE_Q1]
#define UKF_Q2			x[UKF_STATE_Q2]
#define UKF_Q3			x[UKF_STATE_Q3]
#define UKF_Q4			x[UKF_STATE_Q4]
class nav_kf
{

public:

	nav_kf(float *states);
	~nav_kf();
	
	void GpsPosUpdate(vector_t pos, vector_t posLoc, float noises);
	void ZeroRate(vector_t rate);
	void ZeroVel(void);

	void InertialUpdate(vector_t acc, vector_t gyo, vector_t y, float dt);

	float *GetPtrState(void);
	float *GetPtrCovarMatrix(void);

	void SetInitVariance(float DispVel, float DispPos, float DispAccBias, float DispGyoBias, float DispQuat);
	void SetProcVariance(float processDispVel, float processDispRate, float processDispAccBias,	float processDispGyoBias);



	float *x;			// states
private:

	srcdkf_low *srcdkf;
	float *correctState;
	int cntInitQuat;

	void KFInitState(float *states);
	bool QuatInit(vector_t accv, vector_t gyo, vector_t y);
};

#endif