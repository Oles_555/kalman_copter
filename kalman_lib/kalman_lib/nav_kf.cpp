﻿#include "nav_kf.h"
#include "math.h"
#include "cstdlib"
extern "C"
{
#include "../../kalman_core/mat_op.h"
}

void TimeUpdate(float *in, float *noise, float *out, float *u, float dt, int n) {
	float tmp[3], acc[3];
	float rate[3];
	float mat3x3[3 * 3];
	float q[4];
	int i;
	float qRate[4], qRes[4];

	// assume out == in
	out = in;

	for (i = 0; i < n; i++) 
	{
		// create rot matrix from current quat
		q[0] = in[UKF_STATE_Q1*n + i];
		q[1] = in[UKF_STATE_Q2*n + i];
		q[2] = in[UKF_STATE_Q3*n + i];
		q[3] = in[UKF_STATE_Q4*n + i];
		quatToMatrix(mat3x3, q, 1);

		// pos
		out[UKF_STATE_POSX*n + i] = in[UKF_STATE_POSX*n + i] + in[UKF_STATE_VELX*n + i] * dt;
		out[UKF_STATE_POSY*n + i] = in[UKF_STATE_POSY*n + i] + in[UKF_STATE_VELY*n + i] * dt;
		out[UKF_STATE_POSZ*n + i] = in[UKF_STATE_POSZ*n + i] + in[UKF_STATE_VELZ*n + i] * dt;

		// acc
		tmp[0] = u[0] + in[UKF_STATE_ACC_BIAS_X*n + i];
		tmp[1] = u[1] + in[UKF_STATE_ACC_BIAS_Y*n + i];
		tmp[2] = u[2] + in[UKF_STATE_ACC_BIAS_Z*n + i];

		// rotate acc to world frame
		rotateVecByMatrix(acc, tmp, mat3x3);
		acc[1] -= GRAVITY;


		out[UKF_STATE_VELX*n + i] = in[UKF_STATE_VELX*n + i] + acc[0] * dt + noise[UKF_V_NOISE_VEL_X*n + i];
		out[UKF_STATE_VELY*n + i] = in[UKF_STATE_VELY*n + i] + acc[1] * dt + noise[UKF_V_NOISE_VEL_Y*n + i];
		out[UKF_STATE_VELZ*n + i] = in[UKF_STATE_VELZ*n + i] + acc[2] * dt + noise[UKF_V_NOISE_VEL_Z*n + i];


		// rate = rate + bias + noise
		rate[0] = (u[3] + in[UKF_STATE_GYO_BIAS_X*n + i] + noise[UKF_V_NOISE_RATE_X*n + i]) * dt;
		rate[1] = (u[4] + in[UKF_STATE_GYO_BIAS_Y*n + i] + noise[UKF_V_NOISE_RATE_Y*n + i]) * dt;
		rate[2] = (u[5] + in[UKF_STATE_GYO_BIAS_Z*n + i] + noise[UKF_V_NOISE_RATE_Z*n + i]) * dt;

		// rotate quat
		exp_mapQuat(rate, qRate);
		quatMultiply(qRes, q, qRate);

		out[UKF_STATE_Q1*n + i] = qRes[0];
		out[UKF_STATE_Q2*n + i] = qRes[1];
		out[UKF_STATE_Q3*n + i] = qRes[2];
		out[UKF_STATE_Q4*n + i] = qRes[3];

		// acc bias
		out[UKF_STATE_ACC_BIAS_X*n + i] = in[UKF_STATE_ACC_BIAS_X*n + i] + noise[UKF_V_NOISE_ACC_BIAS_X*n + i] * dt;
		out[UKF_STATE_ACC_BIAS_Y*n + i] = in[UKF_STATE_ACC_BIAS_Y*n + i] + noise[UKF_V_NOISE_ACC_BIAS_Y*n + i] * dt;
		out[UKF_STATE_ACC_BIAS_Z*n + i] = in[UKF_STATE_ACC_BIAS_Z*n + i] + noise[UKF_V_NOISE_ACC_BIAS_Z*n + i] * dt;

		// gbias
		out[UKF_STATE_GYO_BIAS_X*n + i] = in[UKF_STATE_GYO_BIAS_X*n + i] + noise[UKF_V_NOISE_GYO_BIAS_X*n + i] * dt;
		out[UKF_STATE_GYO_BIAS_Y*n + i] = in[UKF_STATE_GYO_BIAS_Y*n + i] + noise[UKF_V_NOISE_GYO_BIAS_Y*n + i] * dt;
		out[UKF_STATE_GYO_BIAS_Z*n + i] = in[UKF_STATE_GYO_BIAS_Z*n + i] + noise[UKF_V_NOISE_GYO_BIAS_Z*n + i] * dt;
	}
}
void navUkfRateUpdate(float *u, float *x, float *noise, float *y) {
	y[0] = -x[UKF_STATE_GYO_BIAS_X] + noise[0];
	y[1] = -x[UKF_STATE_GYO_BIAS_Y] + noise[1];
	y[2] = -x[UKF_STATE_GYO_BIAS_Z] + noise[2];
}
void navUkfPosUpdate(float *u, float *x, float *noise, float *y) {	
	float vglov[3];	

	rotateVectorByQuat(vglov, &u[0], &x[UKF_STATE_Q1]);
	
	y[0] = x[UKF_STATE_POSX] + vglov[0] + noise[0]; // return position
	y[1] = x[UKF_STATE_POSY] + vglov[1] + noise[1];
	y[2] = x[UKF_STATE_POSZ] + vglov[2] + noise[2];
}
void navUkfVelUpdate(float *u, float *x, float *noise, float *y) {

	y[0] = x[UKF_STATE_VELX] + noise[0]; 
	y[1] = x[UKF_STATE_VELY] + noise[1];
	y[2] = x[UKF_STATE_VELZ] + noise[2];

}

nav_kf::nav_kf(float *states) {

	correctState = (float *)malloc((SIM_S) * sizeof(float));

	srcdkf = new srcdkf_low(SIM_S, 3, SIM_V, 3);

	cntInitQuat = 0;

	x = srcdkf->GetState();
	KFInitState(states);


}
nav_kf::~nav_kf()
{
	delete(srcdkf);
	free(correctState);
}

void nav_kf::InertialUpdate(vector_t acc, vector_t gyo, vector_t y, float dt) {	
	float u[6];

	u[0] = acc.x;
	u[1] = acc.y;
	u[2] = acc.z;

	u[3] = gyo.x;
	u[4] = gyo.y;
	u[5] = gyo.z;

	if (QuatInit(acc, gyo, y))	{
		srcdkf->TimeUpdate(u, dt, TimeUpdate);
	}

	normalizeQuat(&x[UKF_STATE_Q1], &x[UKF_STATE_Q1 ]);

}
void nav_kf::ZeroRate(vector_t rate) {
	float noise[3];        // measurement variance
	float y[3];            // measurment(s)

	y[0] = rate.x;
	y[1] = rate.y;
	y[2] = rate.z;

	noise[0] = 0.0000001f;
	noise[1] = 0.0000001f;
	noise[2] = 0.0000001f;

	srcdkf->MeasurementUpdate(0, y, 3, 3, noise, navUkfRateUpdate);
}
void nav_kf::ZeroVel(void) {
	float y[3];
	float noise[3];	
	float u[1];

	noise[0] = 1e-7f;
	noise[1] = 1e-7f;
	noise[2] = 1e-7f;

	y[0] = 0.0f;
	y[1] = 0.0f;
	y[2] = 0.0f;


	srcdkf->MeasurementUpdate((float*)u, y, 3, 3, noise, navUkfVelUpdate);
}
void nav_kf::GpsPosUpdate(vector_t pos, vector_t posLoc, float noises) {
	float y[3];
	float noise[3];
	float u[3];

	u[0] = posLoc.x;
	u[1] = posLoc.y;
	u[2] = posLoc.z;

	y[0] = pos.x;
	y[1] = pos.y;
	y[2] = pos.z;
	   	  
	noise[0] = noises;
	noise[1] = noises;
	noise[2] = noises;

	srcdkf->MeasurementUpdate(u, y, 3, 3, noise, navUkfPosUpdate);
}
void nav_kf::KFInitState(float *states) {

	x[UKF_STATE_VELX] = states[UKF_STATE_VELX];
	x[UKF_STATE_VELY] = states[UKF_STATE_VELY];
	x[UKF_STATE_VELZ] = states[UKF_STATE_VELZ];

	x[UKF_STATE_POSX] = states[UKF_STATE_POSX];
	x[UKF_STATE_POSY] = states[UKF_STATE_POSY];
	x[UKF_STATE_POSZ] = states[UKF_STATE_POSZ];

	x[UKF_STATE_Q1] = states[UKF_STATE_Q1];
	x[UKF_STATE_Q2] = states[UKF_STATE_Q2];
	x[UKF_STATE_Q3] = states[UKF_STATE_Q3];
	x[UKF_STATE_Q4] = states[UKF_STATE_Q4];

	// acc bias
	x[UKF_STATE_ACC_BIAS_X] = states[UKF_STATE_ACC_BIAS_X];
	x[UKF_STATE_ACC_BIAS_Y] = states[UKF_STATE_ACC_BIAS_Y];
	x[UKF_STATE_ACC_BIAS_Z] = states[UKF_STATE_ACC_BIAS_Z];

	// gyo bias
	x[UKF_STATE_GYO_BIAS_X] = states[UKF_STATE_GYO_BIAS_X];
	x[UKF_STATE_GYO_BIAS_Y] = states[UKF_STATE_GYO_BIAS_Y];
	x[UKF_STATE_GYO_BIAS_Z] = states[UKF_STATE_GYO_BIAS_Z];


}
bool nav_kf::QuatInit(vector_t accv, vector_t gyo, vector_t y)
{
	if (cntInitQuat > 100)	
		return true;	
	else {
		float estAcc[3], estDir[3];
		float m[3 * 3];

		float rotError[3];
		float acc[3], dir[3];

		float v0a[3] = { 0.0f, 1.0f, 0.0f };
		float vdir[3] = { 0.0f, 0.0f, 1.0f };

		acc[0] = accv.x;
		acc[1] = accv.y;
		acc[2] = accv.z;

		dir[0] = y.x;
		dir[1] = y.y;
		dir[2] = y.z;

		quatToMatrix(m, &UKF_Q1, 1);

		float angle = atan2f(dir[0], dir[1]);

		// rotate gravity to body frame of reference
		rotateVecByRevMatrix(estAcc, v0a, m);

		normalizeVec3(acc, acc);

		// measured error, starting with ACC vector
		rotError[0] = -(acc[2] * estAcc[1] - estAcc[2] * acc[1]) * 0.5f;
		rotError[1] = -(acc[0] * estAcc[2] - estAcc[0] * acc[2]) * 0.5f;
		rotError[2] = -(acc[1] * estAcc[0] - estAcc[1] * acc[0]) * 0.5f;

		rotateVecByRevMatrix(estDir, vdir, m);

		// measured error, starting with ACC vector
		rotError[0] += -(dir[2] * estDir[1] - estDir[2] * dir[1]) * 0.5f;
		rotError[1] += -(dir[0] * estDir[2] - estDir[0] * dir[2]) * 0.5f;
		rotError[2] += -(dir[1] * estDir[0] - estDir[1] * dir[0]) * 0.5f;

		rotateQuat(&UKF_Q1, &UKF_Q1, rotError);
		normalizeQuat(&UKF_Q1, &UKF_Q1);

		x[UKF_STATE_Q1] = UKF_Q1;
		x[UKF_STATE_Q2] = UKF_Q2;
		x[UKF_STATE_Q3] = UKF_Q3;
		x[UKF_STATE_Q4] = UKF_Q4;
		

		cntInitQuat++;

		return false;
	}


}
void nav_kf::SetInitVariance(float DispVel, float DispPos, float DispAccBias, float DispGyoBias, float DispQuat)
{
	float *Q = (float *)malloc((SIM_S) * sizeof(float));

	Q[UKF_STATE_VELX] = DispVel;
	Q[UKF_STATE_VELY] = DispVel;
	Q[UKF_STATE_VELZ] = DispVel;

	Q[UKF_STATE_POSX] = DispPos;
	Q[UKF_STATE_POSY] = DispPos;
	Q[UKF_STATE_POSZ] = DispPos;

	Q[UKF_STATE_Q1] = DispQuat;
	Q[UKF_STATE_Q2] = DispQuat;
	Q[UKF_STATE_Q3] = DispQuat;
	Q[UKF_STATE_Q4] = DispQuat;	

	Q[UKF_STATE_ACC_BIAS_X] = DispAccBias;
	Q[UKF_STATE_ACC_BIAS_Y] = DispAccBias;
	Q[UKF_STATE_ACC_BIAS_Z] = DispAccBias;
	Q[UKF_STATE_GYO_BIAS_X] = DispGyoBias;
	Q[UKF_STATE_GYO_BIAS_Y] = DispGyoBias;
	Q[UKF_STATE_GYO_BIAS_Z] = DispGyoBias;

	srcdkf->SetVariance(Q, 0, 0, 0);

	free(Q);

}
void nav_kf::SetProcVariance(float processDispVel, float processDispRate, float processDispAccBias, float processDispGyoBias)
{
	float *V = (float *)malloc((SIM_V) * sizeof(float));

	V[UKF_V_NOISE_ACC_BIAS_X] = processDispAccBias;
	V[UKF_V_NOISE_ACC_BIAS_Y] = processDispAccBias;
	V[UKF_V_NOISE_ACC_BIAS_Z] = processDispAccBias;

	V[UKF_V_NOISE_GYO_BIAS_X] = processDispGyoBias;
	V[UKF_V_NOISE_GYO_BIAS_Y] = processDispGyoBias;
	V[UKF_V_NOISE_GYO_BIAS_Z] = processDispGyoBias;


	V[UKF_V_NOISE_RATE_X] = processDispRate;
	V[UKF_V_NOISE_RATE_Y] = processDispRate;
	V[UKF_V_NOISE_RATE_Z] = processDispRate;

	V[UKF_V_NOISE_VEL_X]  = processDispVel;
	V[UKF_V_NOISE_VEL_Y] = processDispVel;
	V[UKF_V_NOISE_VEL_Z] = processDispVel;


	srcdkf->SetVariance(0, V, 0, 0);

	free(V);
}

float *nav_kf::GetPtrState(void)
{
	
	correctState[UKF_STATE_VELX] = x[UKF_STATE_VELX];
	correctState[UKF_STATE_VELY] = x[UKF_STATE_VELY];
	correctState[UKF_STATE_VELZ] = x[UKF_STATE_VELZ];

	correctState[UKF_STATE_POSX] = x[UKF_STATE_POSX];
	correctState[UKF_STATE_POSY] = x[UKF_STATE_POSY];
	correctState[UKF_STATE_POSZ] = x[UKF_STATE_POSZ];
	
	correctState[UKF_STATE_ACC_BIAS_X] = x[UKF_STATE_ACC_BIAS_X];
	correctState[UKF_STATE_ACC_BIAS_Y] = x[UKF_STATE_ACC_BIAS_Y];
	correctState[UKF_STATE_ACC_BIAS_Z] = x[UKF_STATE_ACC_BIAS_Z];
	correctState[UKF_STATE_GYO_BIAS_X] = x[UKF_STATE_GYO_BIAS_X];
	correctState[UKF_STATE_GYO_BIAS_Y] = x[UKF_STATE_GYO_BIAS_Y];
	correctState[UKF_STATE_GYO_BIAS_Z] = x[UKF_STATE_GYO_BIAS_Z];

	correctState[UKF_STATE_Q1] = x[UKF_STATE_Q1];
	correctState[UKF_STATE_Q2] = x[UKF_STATE_Q2];
	correctState[UKF_STATE_Q3] = x[UKF_STATE_Q3];
	correctState[UKF_STATE_Q4] = x[UKF_STATE_Q4];

	return correctState;
}

float* nav_kf::GetPtrCovarMatrix(void) {
	return srcdkf->getCovarMatrix();
}

