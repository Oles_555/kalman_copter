using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class control : MonoBehaviour
{
    public Rigidbody body;
    public float k, d;
    public float kStab, kDif, kAnti, d1, kYaw;
    public float kSpeed, kYawRate;

    private float yaw = 90;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    private void FixedUpdate() { 
        var dif = transform.position - body.position;
        Vector3 force = dif * k - body.velocity*d;
       
        body.AddForce(force);

        //dif.y = 0;
        var torqueDif = Vector3.Cross(Vector3.up, dif);
        var torqueAnti = Vector3.Cross(Vector3.up, body.velocity);


        var difQuat = Quaternion.Inverse(transform.rotation) * body.rotation;
        var angle = difQuat.eulerAngles.y;
        if (angle > 180)
            angle -= 360;
        Vector3 torque_yaw = new Vector3(0, -angle, 0);

        var est = body.rotation * Vector3.up;
        Vector3 torque_stab = Vector3.Cross(est, Vector3.up);

        var torque = torque_stab * kStab + torqueDif*kDif + torque_yaw*kYaw - torqueAnti * kAnti - body.angularVelocity * d1;
        body.AddTorque(torque);

        Vector3 controlMarker = new Vector3(0, 0, 0);
        if (Time.timeSinceLevelLoad > 3)
        {
            controlMarker.x = -Input.GetAxis("Horizontal") * kSpeed;
            controlMarker.z = Input.GetAxis("Vertical") * kSpeed;
            controlMarker.y = Input.GetAxis("Up") * kSpeed;

            yaw += Input.GetAxis("Yaw") * kYawRate;
        }


        transform.position += transform.rotation * controlMarker *Time.deltaTime;

        transform.rotation = Quaternion.AngleAxis(yaw, Vector3.up);

    }
}
