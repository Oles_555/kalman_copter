using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IMUModel : MonoBehaviour
{
    public Vector3 Acc, Gyo;
    public Vector3 Pos, Vel;
    public Quaternion Rot;
    public bool activeNoise;
    public bool activeBias;

    public float StanDeviNoiseAcc;
    public Vector3 biasAcc;
    public Vector3 missiligmentAcc;
    public Vector3 scaleAcc;


    public float StanDeviNoiseGyo;
    public Vector3 biasGyo;
    public Vector3 missiligmentGyo;
    public Vector3 scaleGyo;



    private Rigidbody body;
    private Vector3 lastVel;
    private Vector3 AccClear;
    private Vector3 GyoClear;
    private System.Random rand1;



    //methods for generation of noise
    private float normrand()
    {
        float x = 0.0f;
        for (short i = 0; i < 12; i++)
            x += (float)rand1.Next(0, 32767) / 32767.0f;
        return x - 6.0f;
    }

    private float gausNoise(float SD)
    {
        float val = normrand() * SD;
        return (Mathf.Abs(val) > 3.0f * SD) ? 3.0f * SD : val;
    }

    void Start()
    {
        body = GetComponent<Rigidbody>();
        rand1 = new System.Random();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    private void FixedUpdate()
    {
        Vector3 AccBias, GyoBias;

        float dt = Time.deltaTime;
        Rot = body.rotation;
        Pos = body.position;
        Vel = body.velocity;

        GyoClear = Quaternion.Inverse(body.rotation) * body.angularVelocity;
        AccClear = Quaternion.Inverse(body.rotation) * ((body.velocity - lastVel) / dt - Physics.gravity);
        lastVel = body.velocity;

        if (activeBias)
        {                   
            AccBias.x = AccClear.x * scaleAcc.x + AccClear.y * missiligmentAcc.x + AccClear.z * missiligmentAcc.z + biasAcc.x;
            AccBias.y = AccClear.x * missiligmentAcc.x + AccClear.y * scaleAcc.y + AccClear.z * missiligmentAcc.y + biasAcc.y;
            AccBias.z = AccClear.x * missiligmentAcc.z + AccClear.y * missiligmentAcc.y + AccClear.z * scaleAcc.z + biasAcc.z;

            GyoBias.x = GyoClear.x * scaleGyo.x + GyoClear.y * missiligmentGyo.x + GyoClear.z * missiligmentGyo.z + biasGyo.x;
            GyoBias.y = GyoClear.x * missiligmentGyo.x + GyoClear.y * scaleGyo.y + GyoClear.z * missiligmentGyo.y + biasGyo.y;
            GyoBias.z = GyoClear.x * missiligmentGyo.z + GyoClear.y * missiligmentGyo.y + GyoClear.z * scaleGyo.z + biasGyo.z;
        }
        else
        {
            GyoBias = GyoClear;
            AccBias = AccClear;
        }


        if (activeNoise)
        {           
            AccBias.x += gausNoise(StanDeviNoiseAcc);
            AccBias.y += gausNoise(StanDeviNoiseAcc);
            AccBias.z += gausNoise(StanDeviNoiseAcc);

            GyoBias.x += gausNoise(StanDeviNoiseGyo);
            GyoBias.y += gausNoise(StanDeviNoiseGyo);
            GyoBias.z += gausNoise(StanDeviNoiseGyo);

        }

        Acc = AccBias;
        Gyo = GyoBias;
        

    }
}
