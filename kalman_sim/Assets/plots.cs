using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Runtime.InteropServices;
using UnityEngine.SceneManagement;


class plot
{
    public plot(DD_DataDiagram diagram, string n, Func<float> ptr, Color c)
    {
        value = ptr;
        name = n;
        line = diagram.AddLine(name, c);
    }

    public GameObject line;
    public string name;
    public Func<float> value;
}

public class plots : MonoBehaviour
{
    public KalmanFilter kf;
    public IMUModel imu;
    //public bool is_bias;

    private bool is_biasLast = false;
    private DD_DataDiagram m_DataDiagram;


    List<plot> lineList = new List<plot>();


    // Start is called before the first frame update
    void Start()
    {
        GameObject dd = GameObject.Find("DataDiagram");
        if (null == dd)
        {
            Debug.LogWarning("can not find a gameobject of DataDiagram");
            return;
        }
        m_DataDiagram = dd.GetComponent<DD_DataDiagram>();
        //Time.timeScale = 0;
    }

    public void AddGyoBias()
    {
        foreach (var linee in lineList)
            m_DataDiagram.DestroyLine(linee.line);
        lineList.Clear();

        lineList.Add(new plot(m_DataDiagram, "EST_gyoBiasX", () => -kf.gyoBias.x, new Color(0.8f, 0, 0)));
        lineList.Add(new plot(m_DataDiagram, "EST_gyoBiasY", () => -kf.gyoBias.y, new Color(0, 0.8f, 0)));
        lineList.Add(new plot(m_DataDiagram, "EST_gyoBiasZ", () => -kf.gyoBias.z, new Color(0, 0, 0.8f)));

        lineList.Add(new plot(m_DataDiagram, "REF_gyoBiasX", () => imu.biasGyo.x, new Color(1.0f, 0, 0)));
        lineList.Add(new plot(m_DataDiagram, "REF_gyoBiasY", () => imu.biasGyo.y, new Color(0, 1.0f, 0)));
        lineList.Add(new plot(m_DataDiagram, "REF_gyoBiasZ", () => imu.biasGyo.z, new Color(0, 0, 1.0f)));

    }
    public void AddAccBias()
    {
        foreach (var linee in lineList)
            m_DataDiagram.DestroyLine(linee.line);
        lineList.Clear();

        lineList.Add(new plot(m_DataDiagram, "EST_accBiasX", () => -kf.accBias.x, new Color(0.8f, 0, 0)));
        lineList.Add(new plot(m_DataDiagram, "EST_accBiasY", () => -kf.accBias.y, new Color(0, 0.8f, 0)));
        lineList.Add(new plot(m_DataDiagram, "EST_accBiasZ", () => -kf.accBias.z, new Color(0, 0, 0.8f)));


        lineList.Add(new plot(m_DataDiagram, "REF_accBiasX", () => imu.biasAcc.x, new Color(1.0f, 0, 0)));
        lineList.Add(new plot(m_DataDiagram, "REF_accBiasY", () => imu.biasAcc.y, new Color(0, 1.0f, 0)));
        lineList.Add(new plot(m_DataDiagram, "REF_accBiasZ", () => imu.biasAcc.z, new Color(0, 0, 1.0f)));

    } 
    public void AddPos()
    {
        foreach (var linee in lineList)
            m_DataDiagram.DestroyLine(linee.line);
        lineList.Clear();

        lineList.Add(new plot(m_DataDiagram, "EST_posX", () => kf.pos.x, new Color(0.8f, 0, 0)));
        lineList.Add(new plot(m_DataDiagram, "EST_posY", () => kf.pos.y, new Color(0, 0.8f, 0)));
        lineList.Add(new plot(m_DataDiagram, "EST_posZ", () => kf.pos.z, new Color(0, 0, 0.8f)));


        lineList.Add(new plot(m_DataDiagram, "REF_posX", () => imu.Pos.x, new Color(1.0f, 0, 0)));
        lineList.Add(new plot(m_DataDiagram, "REF_posY", () => imu.Pos.y, new Color(0, 1.0f, 0)));
        lineList.Add(new plot(m_DataDiagram, "REF_posZ", () => imu.Pos.z, new Color(0, 0, 1.0f)));
    }
    public void AddVel()
    {
        foreach (var linee in lineList)
            m_DataDiagram.DestroyLine(linee.line);
        lineList.Clear();

        lineList.Add(new plot(m_DataDiagram, "EST_velX", () => kf.vel.x, new Color(0.8f, 0, 0)));
        lineList.Add(new plot(m_DataDiagram, "EST_velY", () => kf.vel.y, new Color(0, 0.8f, 0)));
        lineList.Add(new plot(m_DataDiagram, "EST_velZ", () => kf.vel.z, new Color(0, 0, 0.8f)));


        lineList.Add(new plot(m_DataDiagram, "REF_velX", () => imu.Vel.x, new Color(1.0f, 0, 0)));
        lineList.Add(new plot(m_DataDiagram, "REF_velY", () => imu.Vel.y, new Color(0, 1.0f, 0)));
        lineList.Add(new plot(m_DataDiagram, "REF_velZ", () => imu.Vel.z, new Color(0, 0, 1.0f)));
    }
    public void AddAngles()
    {
        foreach (var linee in lineList)
            m_DataDiagram.DestroyLine(linee.line);
        lineList.Clear();

        lineList.Add(new plot(m_DataDiagram, "EST_anglesX", () => { var val = kf.quat.eulerAngles.x; return val > 180 ? val - 360 : val; }, new Color(0.8f, 0, 0)));
        lineList.Add(new plot(m_DataDiagram, "EST_anglesY", () => { var val = kf.quat.eulerAngles.y; return val > 180 ? val - 360 : val; }, new Color(0, 0.8f, 0)));
        lineList.Add(new plot(m_DataDiagram, "EST_anglesZ", () => { var val = kf.quat.eulerAngles.z; return val > 180 ? val - 360 : val; }, new Color(0, 0, 0.8f)));


        lineList.Add(new plot(m_DataDiagram, "REF_anglesX", () => { var val = imu.Rot.eulerAngles.x; return val > 180 ? val - 360 : val; }, new Color(1.0f, 0, 0)));
        lineList.Add(new plot(m_DataDiagram, "REF_anglesY", () => { var val = imu.Rot.eulerAngles.y; return val > 180 ? val - 360 : val; }, new Color(0, 1.0f, 0)));
        lineList.Add(new plot(m_DataDiagram, "REF_anglesZ", () => { var val = imu.Rot.eulerAngles.z; return val > 180 ? val - 360 : val; }, new Color(0, 0, 1.0f)));
    }
    public void ReloadScene()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        
    }
    public void StartTime()
    {
        Time.timeScale = 1;
    }


    private void FixedUpdate()
    {
        //if (is_biasLast == false && is_bias)
        //    AddGyoBias();
        //is_biasLast = is_bias;

        foreach (var linee in lineList)
            m_DataDiagram.InputPoint(linee.line, new Vector2(Time.deltaTime, (float)linee.value.Invoke()));        


    }
}
